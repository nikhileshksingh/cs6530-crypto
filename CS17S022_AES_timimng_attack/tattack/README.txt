CS6530 Applied Cryptography
Jan 2018
Tutorial 4.


---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

The answers to the questionnare are included in 'Report.pdf' file.
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------





---------------------------------------------------------------------------------------
You need to mount a cache timing attack on AES by measuring its execution time
over a large number of encryptions.

This code will compile on a 64 bit Linux, Apple, machine with gcc.
It has not been tested on Windows.
---------------------------------------------------------------------------------------

Step 0: Write your group name roll number  in the preamble of attack.c

Step 1: Determine the size of a cache line on your Linux system.
Assuming Ubuntu 64 bit platform, this can be obtained from 
/sys/devices/system/cpu/cpuX/cache', where 'X' is the core number (takes values 0,1,2,...)
See: http://arbidprobramming.blogspot.in/2012/04/all-about-cache-memory-in-linux.html

Step 2: Determine the number of T-table elements that fit in a memory block.
a. Call this value N. 
Note each T-table entry is defined u32 and 1 memory block = 1 cache line.
Then determine the maximum number of memory blocks that the T-table can be present in. 
b. Call this N_MEM_BLOCKS.
Then determine log_2_N.
c. Call this LOG_CLINE_SIZE


Step 3: Compute the average execution time for AES on your system by measuring over
10,000 encryptions with random plaintexts and then set the value of THRESHOLD.
The THRESHOLD will be roughly twice the average encryption time.

Step 4: Set the value of a key in the file 'key'. A sample is present in the
directory. This is the AES secret key.

Step 5: Fill in the function finddeviant. The steps are present.

Step 6: 
Make the AES object file
$cd lib; make clean; make;
$cd ..
Make and run the attack code
$make clean; make; ./attack 
to run the program.

Step 7: Repeat experiment 10 times and compute the average attack success.


-------------------------- Questionaire ---------------------------------
---- to be submitted through moodle along with the code.

1. Run the command cp /proc/cpuinfo > ./cpuinfo.
This file should be uploaded to moodle.

2. What is the size of a cache line in your system?

3. What is N, N_MEM_BLOCKS, LOG_CLINE_SIZE?

4. What is the average execution time of your AES code?

5. What is the value of TIME_THRESHOLD set in your code?

6. What is the number of iterations set?

7. What is the key that you set?

8. What is the output expected for that key?

9. What is the average attack success over 10 runs?
   This should be the average of (10 * 12 = 120 outputs)

10. Explain in your own words why the attack works.

11. If you have any other comments, observations, inferences, write them down here.


12. Create a file in the following format
    key : ( in hex bytes seperated by spaces) ... copy from the file key
    outuput 1 : output of first run (the last line printed on the screen)
    outuput 2 : output of 2nd run (the last line printed on the screen)
    .
    .
    outuput 10 : output of 10th run (the last line printed on the screen)
--------------------------------------------------------------------------

