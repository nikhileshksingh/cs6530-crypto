/******************************************************************************

 * Names        : Nikhilesh K. Singh | CS17S022
 ******************************************************************************/
#include <stdio.h>
#include <math.h>
#include <string.h>
#include <stdlib.h>

#include <aes.h>

#include "params.h"


#define ITERATIONS (1 << 24)              /* The maximum iterations for making the statistics */

AES_KEY expanded;

/* CS6530 : Execution time measurements could be considerably noisy, due
 * other system events like interrupts, page faults, multi-core envrionments, and
 * scheduling context. We remove the extremely noisy measurements by a threshold.
 * If the execution time of AES is above the threshold, then do not consider it in the average 
 * TIME_THRESHOLD should be approximately 1.5 to 2 times the average execution time */


#define TIME_THRESHOLD     2500   // for 4 tables of 1024 bytes

/* CS6530  : Determine the number of elements that can be stored in a cache line.
 * Then determine the maximum number of blocks the table can take */
#define N_MEM_BLOCKS        16
#define LOG_CLINE_SIZE      4

unsigned char pt[16];               /* Holds the Plaintext */
unsigned char ct[16];               /* Holds the ciphertext */

unsigned int ttime[16][N_MEM_BLOCKS];         /* Holds the timing:ttime[x][y], The total time when pt[x] = y     */
unsigned int tcount[16][N_MEM_BLOCKS];        /* Holds the count: tcount[x][y], How often is pt[x] = y occurs      */
double tavg[16][N_MEM_BLOCKS];                /* ttime[x]/tcount[x]  average execution time for pt[x] = y */
double deviations[16][N_MEM_BLOCKS];          /* Deviations from tavgavg */
double tavgavg;                     /* The overall average of all timings */

extern unsigned timestamp();
extern void cleancache();

void printtime()
{
	int i, c;
	FILE *f;

	f = fopen("log", "w");
	for(c=4; c<16; ++c){
		fprintf(f, ".............%d.........\n", c);
		for(i=0; i<N_MEM_BLOCKS; ++i){
			fprintf(f, "%d  %.3f  %.4f\n", i, tavg[c][i], deviations[c][i]);
		}
	}
	fclose(f);
}

unsigned int finddeviant(unsigned int c)
{
    int i, maxi=0;
    double ttimesum = 0, tcountsum = 0;
    double maxdeviation;
    double temp, sum=0;

    /* CS6530 (Step 3) : 
     * 1. compute average timing for c (tavg[c][i]) for all possible values of i (0 to N_MEM_BLOCKS)
         * 2. compute overall average (tavgavg) of all encryptions that has a valid execution time
         * 3. compute deviation of tavg[c][i] from tavgavg
         * 4. Find the value of i for which deviation is maximum. This is maxi, which is returned
         */
        
        //*Step 1*// 
        for(i=0; i<N_MEM_BLOCKS; i++){
                
                if(ttime[c][i] != 0 && tcount[c][i] != 0){
                    tavg[c][i] = ttime[c][i]/tcount[c][i];
                    ttimesum += ttime[c][i];
                    tcountsum += tcount[c][i];
                }
        }

        tavgavg = ttimesum / tcountsum;  //tavgavg for each row //*Step 2*//
        
        maxdeviation = -99999; //just initialising maxdeviation variable
        

        for(i=0;i<N_MEM_BLOCKS;i++){
            temp = abs(tavg[c][i] - tavgavg); //*Step 3*//
            deviations[c][i] = temp;
            
            if(temp > maxdeviation){
                maxdeviation = temp;  //choosing which ever deviation is max 
                maxi = i;             //*Step 4*//
            }
        }


    return maxi;
}


void findkeys()
{
	int c=4;
	for (c=4; c<16; ++c)
		printf("%02d(%x) ", c, finddeviant(c));	
	printf("\n");
}

void attackrnd1()
{
	int ii=0, i;
	unsigned int start, end, timing;

	while(ii++ <= (ITERATIONS)){
		/* Set a random plaintext */
		for(i=4; i<16; ++i) pt[i] = random() & 0xff;
		/* Fix a few plaintext bits of some plaintext bytes */
		pt[0] = 0x0;
		pt[1] = 0x0;
		pt[2] = 0x0;
		pt[3] = 0x0;

		/* clean the cache memory of any AES data */
		cleancache();	

		/* Make the encryption */
		start = timestamp();
		AES_encrypt(pt, ct, &expanded);
		end = timestamp();

		timing = end - start;

		if(ii > 1000 && timing < TIME_THRESHOLD){      
			/* Record the timings */
			for(i=4; i<16; ++i){
				ttime[i][pt[i] >> LOG_CLINE_SIZE] += timing;
				tcount[i][pt[i] >> LOG_CLINE_SIZE] += 1;	
			}	
			
			/* print if its time */
			if (!(ii & (ii - 1))){
				printf("%08x\t", ii);
				findkeys();
				printtime();
			}
		}
	}
	return;
}


void ReadKey(const char *filename)
{
	int i,x;
	FILE *f;
	unsigned int i_secretkey[16]; 
	unsigned char uc_secretkey[16]; 

	/* Read key from a file */
	if((f = fopen(filename, "r")) == NULL){
		printf("Cannot open key file\n");
		exit(-1);
	}
	printf("The AES key is : ");
	for(i=0; i<16; ++i){
		x = fscanf(f, "%x", &i_secretkey[i]);
		printf("%02x ", i_secretkey[i]);
		uc_secretkey[i] = (unsigned char) i_secretkey[i];
	}
	fclose(f);
	printf("\n");
	AES_set_encrypt_key(uc_secretkey, 128, &expanded);
}

//* Function to calculate average time taken by AES of 1<<24 random plaintexts*//
//* This helped to determine a suitable value for the threshold*// 

/* void computeThreshold(){

	int ii=0, i;
	unsigned int start, end, timing;
	float avg = 0.0;

 	while(ii++ <= (ITERATIONS)){
		/* Set a random plaintext */
/*		for(i=0; i<16; ++i) pt[i] = random() & 0xff;
        
		printf("\n**** %d PLAINTEXT****\n", ii);
		for(i=0; i<16; ++i) {
			printf(" %c ",pt[i]);}



		/* Make the encryption */
/*		start = timestamp();
		AES_encrypt(pt, ct, &expanded);
		end = timestamp();

		timing = end - start;
		printf("\n time taken for encryption : %d \n", timing);
        avg += timing;


	}
	avg = avg/(ITERATIONS);
	printf("\n avg = %f \n", avg);

} */

/* 
 * The main 
 */
int main(int argc, char **argv)
{
	srandom(timestamp());

	ReadKey((const char *)"key");
	printf("Getting First Round Key Relations\n");
       
	attackrnd1();
}

