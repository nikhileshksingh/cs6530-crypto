#include <cstdio>
#include<iostream>
#include<bits/stdc++.h>
#include<stdint.h>
#include <string.h>
#include <stdlib.h>
#include <ctime>
//#include <chrono>
//using namespace std::chrono
 



#include "KeyExp.h"
#include "lynxenc.h"

using namespace std;

#define MAX_CHAR_FOR_FILE_OPERATION 1000000
#define CHUNK 16/* read 16 bytes at a time */


int main(int argc, char* argv[])
{

    
    unsigned int start, end, timing;
    //BYTE bKey[16]	= { 0x00,0x01,0x02,0x03,0x04,0x05,0x06,0x07,0x08,0x09,0x0A,0x0B,0x0C,0x0D,0x0E,0x0F }; //Testing
    //BYTE bInput[16] = { 0x00,0x11,0x22,0x33,0x44,0x55,0x66,0x77,0x88,0x99,0xAA,0xBB,0xCC,0xDD,0xEE,0xFF }; //testing
    BYTE bKey[16];
    
    
    int x;
    FILE *f;
    unsigned int i_secretkey[16]; 
	unsigned char uc_secretkey[16]; 

    /* Read key from a file */
	if((f = fopen((const char *)"key", "r")) == NULL){
		printf("Cannot open key file\n");
		exit(-1);
	}

    for(int i=0; i<16; ++i){
		x = fscanf(f, "%x", &i_secretkey[i]);
		bKey[i] = (unsigned char)i_secretkey[i];
        
		
	}
	fclose(f);
    
    printf("Plaintext : alice.txt \n\n");


    /*Key from File*/
    printf("\n\nKey\n");
    for(int i = 1; i<=16; i++){
        printf("%02X%c", bKey[i-1], i % 4 == 0 ? '\n' : ' ');
    }
    
    printf("\n\n");

    BYTE bExpKey[176];

    KeyExpansion(bKey, bExpKey);
    ComputeTBoxes();
    BYTE bInput[CHUNK];



    int y, numRead;
    BYTE buffer[16];
    FILE *fp = fopen("alice.txt", "rb");
    fseek(fp, 0, SEEK_END);
    int fileSize = ftell(fp);
    fseek(fp, 0, SEEK_SET);

    //    printf("\n FileSize %d \n\n\n", fileSize);
 
    printf("CipherText : \n\n");
    for(x = 0; y < fileSize; y += 16) {
        numRead = fread(buffer, 1, 16, fp);
        if (numRead < 1) {
            printf("error\n");
            break;
        }
        if (numRead < 16) {
            memset(&buffer[numRead], 0, 16-numRead);
        }
        //printf("%.*s\n\n", numRead,buffer);

        //auto start = high_precission_clock::now();
        
        lynx(buffer, bExpKey);
        
        //auto stop = high_precission_clock()::now;
        
        for(int j=0; j<16; j++){
        //printf("%02X%c" , buffer[j], j % 4 == 0 ? '\n' : ' ');
        printf("%02X " , buffer[j]);
        }
    }
    
    
    //auto duration = duration_cast<microseconds>(stop - start);
    //cout << duration.count() << endl;






/*     int orgLen = strlen((const char*)buf);
    int lenPad = orgLen;

    if(lenPad % 16 != 0)
        lenPad = (lenPad / 16 + 1) * 16;

    unsigned char* bInput = new unsigned char[lenPad];
    
    for(int i=0; i<lenPad; i++){
        if(i >= orgLen) bInput[i] = 0;
        else bInput[i] = buf[i];


    } */


    return 0;
}


