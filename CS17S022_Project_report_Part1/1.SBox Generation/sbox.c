#include<stdio.h>
#include<stdint.h>

 #include <unistd.h>

//bit shift, source: Wikipedia
#define ROTL8(x,shift) ((uint8_t) ((x) << (shift)) | ((x) >> (8 - (shift)))) 


//SBOX using the irreducible polynomils x^8+x^7+x^6+x^3+x^2+x+1
//Arithmetic Operations in the GF(2^8) using irreducible polynomils x^8+x^7+x^6+x^3+x^2+x+1

//Nikhilesh K. Singh
//CS17S022
//Course: CS6530 | IIT Madras
//2018



//* Addition in Galois field GF(2^8) is actually XOR operation *//

uint8_t gadd(uint8_t a, uint8_t b) {
	return a ^ b;
}

//* Subtraction in GF(2^8) is same as addition, i.e., XOR *//

uint8_t gsub(uint8_t a, uint8_t b) {
	return a ^ b;
}

//* Multiplication result may overflow, thus taking modulus with the irreducible polynomial: 0x1CF *//
uint8_t gmul(uint8_t a, uint8_t b) {
	uint8_t p = 0;
	
    while (a && b) {
        if (b & 1) 
            p ^= a;

        if (a & 0x80) //overflow
            a = (a << 1) ^ 0x1cf; //0x1cf is the irreducible polynomial 
            else
                a <<= 1;
            b >>= 1;
	}

	return p;
}

//* Finding Multiplicative inverese  *//
uint8_t ginv(uint8_t val)
{
	uint8_t a=1;
	uint8_t x=0;

	//* Trying out all the values by brute force to find the inverse *//
	for(a=1;a<=255;a++)
	{
		x = gmul(val,a);
		if(x==1)
			break;

	}
	if(x==1)
		return a;
	else
		return 0;
}

/* SBox : Multiplicative inverse -> Affine transformation*/

void sbox() {
	uint8_t sbox[256];
	uint8_t p = 1, q = 1;
	
	for(p=1; p<=254; p++) {
		q=ginv(p);

		uint8_t trans = q ^ ROTL8(q, 1) ^ ROTL8(q, 4) ^ ROTL8(q, 6) ^ ROTL8(q, 7);

		sbox[p] = trans ^ 0xc9;
	} 


	q=ginv(255); //Multiplicative inverse

		// affine transformation 
		uint8_t trans = q ^ ROTL8(q, 1) ^ ROTL8(q, 4) ^ ROTL8(q, 6) ^ ROTL8(q, 7);

		sbox[255] = trans ^ 0xc9;

	/* 0 is a special case since it has no inverse */
	sbox[0] = 0xc9; // This value was chosen randomly by trying out several ones
	
	
	printf("\n");
	printf("******************************************************************\n");
    printf("*                                                                *\n");
	printf("*                                                                *\n");
	printf("*                SBOX using irreducible polynomial               *\n");
	printf("*                     x^8+x^7+x^6+x^3+x^2+x+1                    *\n");   
	printf("*                                                                *\n");
	printf("*                                                                *\n");
	printf("******************************************************************\n");
	printf("\n");                                     

	
	sleep(2);



	
	for(int i=0;i<256;i++)
	{
		if(i%16 == 0 && i!=0)
			printf("\n");
		printf("%2x ",sbox[i]);
	
	}

printf("\n");
printf("\n");
printf("\n");
printf("\n");
printf("\n");
printf("\n");
}


int main(int argc, char **argv){

	sbox();
}
