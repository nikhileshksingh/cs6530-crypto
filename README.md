# CS6530-crypto

The repo contains a code, reports and resources for CS6530-Cryptography course. 

It includes:

1. Project Lynx, an alternative AES encryption standard.
2. A cache timing side-channel attack on t_table implementaion of standard AES.