#include<stdio.h>
#include<stdint.h>

int diffTable[256][256];

double latb[256][256]; //bias table

int maxColinRow[256]; // to store maximum biased column in each row
double latmaxVal = 0; //To store maximum bias value in the entire table
int latmaxrow;  //latmaxVal row
int latmaxcol;  //latmaxVal columns

long double bias = 1; //to calculte final bias

uint8_t perfmat[4][4]; //the dummy matrix we will work on

//*function for gmuliplying in Galois field*//
uint8_t gmul(uint8_t a, uint8_t b) {
	uint8_t p = 0;
	
    while (a && b) {
        if (b & 1) 
            p ^= a;

        if (a & 0x80) //overflow
            a = (a << 1) ^ 0x1cf; //0x1cf is the irreducible polynomial 
            else
                a <<= 1;
            b >>= 1;
	}

	return p;
}

uint8_t sBox[256]= {    
	0xc9, 0x1a, 0x95, 0x5e, 0x52, 0x9d, 0x02, 0x9a, 0x04, 0xf8, 0x56, 0xe5, 0x99, 0x19, 0x60, 0xdc, 
    0x2f, 0xe3, 0x51, 0x90, 0x06, 0xeb, 0xdf, 0xa0, 0x54, 0xaa, 0xa1, 0xff, 0xa8, 0x0b, 0x43, 0x53, 
    0x0f, 0xbf, 0x69, 0x41, 0x30, 0xf4, 0xd0, 0xae, 0x9b, 0x12, 0x6d, 0xe8, 0x77, 0xdb, 0xc8, 0x63, 
    0xb2, 0x5a, 0x78, 0x09, 0x48, 0xfe, 0x67, 0xd2, 0xcc, 0xee, 0x1d, 0x44, 0x8c, 0x5f, 0x84, 0xd7, 
    0x1f, 0xc3, 0xf2, 0xd1, 0x2c, 0xcd, 0x38, 0xbb, 0x35, 0x7f, 0x57, 0x2b, 0x45, 0x4d, 0x7a, 0x7c, 
    0xe0, 0x11, 0x91, 0x47, 0x2e, 0x76, 0x59, 0xbe, 0x96, 0x3f, 0x75, 0xb8, 0x49, 0x21, 0x9c, 0xfd, 
    0x74, 0x42, 0x00, 0xc2, 0xa4, 0xb1, 0xa9, 0x98, 0xbc, 0x32, 0xe7, 0x58, 0x9e, 0xec, 0xf1, 0xfb, 
    0x4b, 0xaf, 0xef, 0x2a, 0xa3, 0xac, 0xba, 0xc7, 0xde, 0x26, 0x82, 0x80, 0xda, 0x29, 0x73, 0x1c, 
    0x17, 0x5c, 0x79, 0x6a, 0xe1, 0xd9, 0xc5, 0xb0, 0x3b, 0x8a, 0xcb, 0x4a, 0x31, 0x28, 0xf0, 0x36, 
    0xb7, 0x97, 0x92, 0x14, 0x86, 0x4e, 0x0d, 0x37, 0x3a, 0x15, 0x3e, 0x93, 0x10, 0x3d, 0xa6, 0xea, 
    0x5d, 0x61, 0xa5, 0x4f, 0x50, 0xe9, 0x8e, 0x89, 0x8f, 0x20, 0x16, 0x1b, 0x34, 0xad, 0x72, 0x1e, 
    0x66, 0x01, 0x07, 0x71, 0x22, 0xe2, 0xc4, 0xf3, 0x3c, 0x0e, 0xbd, 0x46, 0xd6, 0x9f, 0xd3, 0x33, 
    0xa2, 0xc0, 0x0c, 0xcf, 0x2d, 0xc1, 0xf9, 0xdd, 0xca, 0x18, 0x40, 0x24, 0x4c, 0xb6, 0xd4, 0x13, 
    0xc6, 0x83, 0x81, 0x55, 0x6b, 0x39, 0xb4, 0xb5, 0x62, 0x08, 0x5b, 0x7d, 0xd5, 0x68, 0x65, 0x05, 
    0x88, 0x03, 0xfa, 0xab, 0x6f, 0x0a, 0x8d, 0x6e, 0xfc, 0x27, 0xce, 0x64, 0x70, 0xe6, 0x7b, 0xf6, 
    0xf7, 0xed, 0x8b, 0x7e, 0x6c, 0xb3, 0xd8, 0x25, 0xf5, 0xe4, 0xb9, 0x87, 0x94, 0xa7, 0x23, 0x85 
	};

int size = 256;

 void findDiff ()
 {
   int x1, x2, dx, dy;
    for (x1 = 0; x1 < size; ++x1) {
        for (dx = 0; dx < size; ++dx) {
            x2 = x1 ^ dx;
            dy = sBox[x1] ^ sBox[x2];
            ++diffTable[dx][dy];
        }
    }

}



void shiftRows(){
    //done the shifting on paper and hardcoding the transfromation

    uint8_t tmp[4][4];

    tmp[0][0] = perfmat[0][0];
    tmp[0][1] = perfmat[0][1];
    tmp[0][2] = perfmat[0][2];
    tmp[0][3] = perfmat[0][3]; 

    tmp[1][0] = perfmat[1][1];
    tmp[1][1] = perfmat[1][2];
    tmp[1][2] = perfmat[1][3];
    tmp[1][3] = perfmat[1][0]; 

    tmp[2][0] = perfmat[2][2];
    tmp[2][1] = perfmat[2][3];
    tmp[2][2] = perfmat[2][0];
    tmp[2][3] = perfmat[2][1]; 

    tmp[3][0] = perfmat[3][3];
    tmp[3][1] = perfmat[3][0];
    tmp[3][2] = perfmat[3][1];
    tmp[3][3] = perfmat[3][2]; 

    for(int i=0; i<4; i++){
        for(int j=0; j<4; j++){
            perfmat[i][j] = tmp[i][j];
        } 
    }

}


void mixCol(){
    //gmuliplication by the 2-3-1-1 matrix
    //for multiplaction result by 2 and 3, use the table stored as an array
    /*Matrix:

    2 3 1 1 
    1 2 3 1 
    1 1 2 3 
    3 1 1 2
    */
    uint8_t tmp[4][4];
    
    /*Matrix multiplication
    First element = first col * first row and addition (XOR) over it
    */
   

    tmp[0][0] = (uint8_t)(gmul(2,perfmat[0][0]) ^ gmul(3,perfmat[1][0]) ^ perfmat[2][0] ^ perfmat[3][0]);
    tmp[0][1] = (uint8_t)(gmul(2,perfmat[0][1]) ^ gmul(3,perfmat[1][1]) ^ perfmat[2][1] ^ perfmat[3][1]);
    tmp[0][2] = (uint8_t)(gmul(2,perfmat[0][2]) ^ gmul(3,perfmat[1][2]) ^ perfmat[2][2] ^ perfmat[3][2]);
    tmp[0][3] = (uint8_t)(gmul(2,perfmat[0][3]) ^ gmul(3,perfmat[1][3]) ^ perfmat[2][3] ^ perfmat[3][3]);

    tmp[1][0] = (uint8_t)(perfmat[0][0] ^ gmul(2,perfmat[1][0]) ^ gmul(3,perfmat[2][0]) ^ perfmat[3][0]);
    tmp[1][1] = (uint8_t)(perfmat[0][1] ^ gmul(2,perfmat[1][1]) ^ gmul(3,perfmat[2][1]) ^ perfmat[3][1]);
    tmp[1][2] = (uint8_t)(perfmat[0][2] ^ gmul(2,perfmat[1][2]) ^ gmul(3,perfmat[2][2]) ^ perfmat[3][2]);
    tmp[1][3] = (uint8_t)(perfmat[0][3] ^ gmul(2,perfmat[1][3]) ^ gmul(3,perfmat[2][3]) ^ perfmat[3][3]);

    tmp[2][0] = (uint8_t)(perfmat[0][0] ^ perfmat[1][0] ^ gmul(2,perfmat[2][0]) ^ gmul(3,perfmat[3][0]));
    tmp[2][1] = (uint8_t)(perfmat[0][1] ^ perfmat[1][1] ^ gmul(2,perfmat[2][1]) ^ gmul(3,perfmat[3][1]));
    tmp[2][2] = (uint8_t)(perfmat[0][2] ^ perfmat[1][2] ^ gmul(2,perfmat[2][2]) ^ gmul(3,perfmat[3][2]));
    tmp[2][3] = (uint8_t)(perfmat[0][3] ^ perfmat[1][3] ^ gmul(2,perfmat[2][3]) ^ gmul(3,perfmat[3][3]));

    tmp[3][0] = (uint8_t)(gmul(3,perfmat[0][0]) ^ perfmat[1][0] ^ perfmat[2][0] ^ gmul(2,perfmat[3][0]));
    tmp[3][1] = (uint8_t)(gmul(3,perfmat[0][1]) ^ perfmat[1][1] ^ perfmat[2][1] ^ gmul(2,perfmat[3][1]));
    tmp[3][2] = (uint8_t)(gmul(3,perfmat[0][2]) ^ perfmat[1][2] ^ perfmat[2][2] ^ gmul(2,perfmat[3][2]));
    tmp[3][3] = (uint8_t)(gmul(3,perfmat[0][3]) ^ perfmat[1][3] ^ perfmat[2][3] ^ gmul(2,perfmat[3][3]));



    for(int i=0; i<4; i++){
        for(int j=0; j<4; j++){
        perfmat[i][j] = tmp[i][j];

    }
}

}

void dispmat(){
     for(int i=0; i<4;i++)
	{
		for(int j=0;j<4;j++)
		{
			printf("%d\t", perfmat[i][j]);
    }
		printf("\n");
	}
printf("\n\n");
}

void lattrans(){
    for(int i=0; i<4;i++)
	{
		for(int j=0;j<4;j++)
		{
			if(perfmat[i][j] != 0){
                bias *= 2*latb[perfmat[i][j]][maxColinRow[perfmat[i][j]]];
                
                perfmat[i][j] = maxColinRow[perfmat[i][j]];
                
            }
    }
		
	}
    //printf("******************** %e ********************", bias);

}

int main(int argc, char **argv){

	findDiff();
	for(int i=0; i<size;i++)
	{
		for(int j=0;j<size;j++)
		{
			latb[i][j] =  diffTable[i][j];
			latb[i][j] =  latb[i][j]/256;
		}
		
	}

//finding latmax
	for(int i=1; i<size;i++)
	{
		for(int j=1;j<size;j++)
		{
			if (fabs(latb[i][j]) > fabs(latmaxVal)){
          latmaxVal = latb[i][j];
          latmaxrow = i;
          latmaxcol = j;
            }
        }
    }
    
    //printf("%lf %d %d", latmaxVal, latmaxrow, latmaxcol);

    for(int i = 0 ; i<size;i++)
        maxColinRow[i] = 0;

    //finding max in each row
	for(int i=1; i<size;i++)
	{
		for(int j=1;j<size;j++)
		{
			if ( fabs(latb[i][j]) > fabs(latb[i][maxColinRow[i]])){
                maxColinRow[i] = j;
        }
    }

	}


    for(int i=0; i<4; i++){
        for(int j=0;j<4; j++ ){
            perfmat[i][j] = 0;
        }
    }



perfmat[0][0] = latmaxcol;
bias *= latb[latmaxrow][latmaxcol];


//*Code the print the maximum biased column in every row
/* for(int i = 0; i<256; i++){

    printf("%d ", maxColinRow[i]);
}
printf("\n "); */

dispmat();

shiftRows();
printf("***After 1st ShiftRow***\n");
dispmat();


mixCol();
printf("***After 1st MixCol***\n");
dispmat();

lattrans();
printf("***Getting the corresponding columns based on DDT***\n");
dispmat();

shiftRows();
printf("***After 2nd ShiftRow***\n");
dispmat();

mixCol();
printf("***After 2nd MixCol***\n");
dispmat();

lattrans();
printf("***Getting the corresponding columns based on DDT***\n");
dispmat();


printf("\n Bias of the deadliest trail : ");
printf("%e \n", bias);





return 0;
}
