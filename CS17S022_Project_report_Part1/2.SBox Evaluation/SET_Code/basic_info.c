#include "header.h"


void header()
{
	printf("\n/***************************************************************************\n\n");
	printf("\t\t\tSET - S-box Evaluation Toolbox\n\n");
	printf("\nVersion 0.9.");
	
	printf("\n***************************************************************************/\n");
}

void s_box_info (char *name)
{
	printf("\nSBox to evaluate: %s\n\n", name);

}


double get_time (clock_t finish, clock_t begin)
{
	double value;
	value = (double)(finish - begin) / CLOCKS_PER_SEC * 1000;
	printf("\nCalculations took %.2lf miliseconds to run\n\n", value);
	return value;
}
